package main

import (
	"context"
	"github.com/sirupsen/logrus"
	"myfresh/internal/database"
	"myfresh/internal/options"
	"myfresh/internal/server"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	opts, err := options.Parse()
	if err != nil {
		logrus.WithError(err).Fatal("Failed to parse options")
	}

	db, err := database.NewDB(&opts.DB)
	if err != nil {
		logrus.WithError(err).Fatal("Failed to connect database")
	}

	s := server.NewServer(&opts.HTTP, db)

	term := make(chan os.Signal)
	signal.Notify(term, os.Interrupt, syscall.SIGTERM)
	select {
	case <-s.Start():
	case <-term:
		logrus.Info("Receiving SIGTERM, exiting gracefully...")
	}

	s.Stop(context.Background(), 30*time.Second)

	logrus.Info("Server stopped")
}
