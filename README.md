## Run tests

```
docker-compose up -d
go test ./...
```

## Run server

```
docker-compose up -d
dbmate --url postgres://staging:staging@localhost:5432/myfresh?sslmode=disable --no-dump-schema -d seed/ up
go run .
```