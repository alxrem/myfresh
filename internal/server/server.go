package server

import (
	"context"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	log "github.com/sirupsen/logrus"
	"myfresh/internal/database"
	"myfresh/internal/graph"
	"myfresh/internal/graph/generated"
	"myfresh/internal/options"
	"myfresh/internal/reqctx"
	"net/http"
	"strings"
	"time"
)

type Server struct {
	httpServer *http.Server
	db         *database.DB

	HandleQuery http.Handler
}

func NewServer(opts *options.HTTP, db *database.DB) *Server {
	mux := http.ServeMux{}
	s := &Server{
		httpServer: &http.Server{
			Addr:    opts.Addr,
			Handler: &mux,
		},
		db: db,
	}

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))
	mux.Handle("/", playground.Handler("GraphQL playground", "/query"))
	mux.Handle("/query", s.WithAuthToken(s.WithDB(srv)))

	return s
}

func (s *Server) Start() <-chan interface{} {
	stopped := make(chan interface{})
	go func() {
		log.Info("Server started")
		if err := s.httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.WithError(err).Warn("Failed to start HTTP server")
		}
		close(stopped)
	}()
	return stopped
}

func (s *Server) Stop(ctx context.Context, gracefulShutdownTimeout time.Duration) {
	shutdownCtx, cancel := context.WithTimeout(ctx, gracefulShutdownTimeout)
	if err := s.httpServer.Shutdown(shutdownCtx); err != nil && err != http.ErrServerClosed {
		log.WithError(err).Warn("HTTP server shutdown error")
	}
	cancel()
}

func (s *Server) WithDB(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r.WithContext(reqctx.WithDB(r.Context(), s.db)))
	})
}

func (s *Server) WithAuthToken(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := func() *http.Request {
			authInfo := strings.Fields(r.Header.Get("Authorization"))
			if len(authInfo) != 2 {
				return r
			}
			if strings.ToLower(authInfo[0]) != "bearer" {
				return r
			}
			return r.WithContext(reqctx.WithAuthToken(r.Context(), authInfo[1]))
		}()

		h.ServeHTTP(w, req)
	})
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.httpServer.Handler.ServeHTTP(w, r)
}
