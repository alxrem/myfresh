package database

import (
	"context"
	"github.com/go-pg/pg/v10"
	"myfresh/internal/options"
)

type DB struct {
	*pg.DB
}

func NewDB(o *options.DB) (*DB, error) {
	opts, err := pg.ParseURL(o.URL)
	if err != nil {
		return nil, err
	}
	db := DB{}

	db.DB = pg.Connect(opts)

	if err := db.Ping(context.Background()); err != nil {
		return nil, err
	}
	return &db, nil
}
