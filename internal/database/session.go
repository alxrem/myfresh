package database

import (
	"time"
)

type Session struct {
	ID         int
	UserId     int
	Expiration time.Time
	Token      string
}
