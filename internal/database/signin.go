package database

import (
	"time"
)

type SignIn struct {
	tableName struct{} `pg:"signins"`

	ID         int64
	Expiration time.Time
	Phone      string
	Code       string
}
