package options

import "github.com/caarlos0/env/v6"

type Options struct {
	DB   DB
	HTTP HTTP
}

type DB struct {
	URL string `env:"DATABASE_URL" envDefault:"postgres://staging:staging@localhost:5432/myfresh?sslmode=disable"`
}

type HTTP struct {
	Addr string `env:"HTTP_ADDR" envDefault:":8080"`
}

func Parse() (*Options, error) {
	opts := Options{}
	if err := env.Parse(&opts); err != nil {
		return nil, err
	}
	return &opts, nil
}
