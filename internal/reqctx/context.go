package reqctx

import (
	"context"
	"myfresh/internal/database"
)

type contextKey int

const (
	dbCtxKey contextKey = iota
	authTokenCtxKey
)

func WithDB(ctx context.Context, db *database.DB) context.Context {
	return context.WithValue(ctx, dbCtxKey, db)
}

func DB(ctx context.Context) *database.DB {
	return ctx.Value(dbCtxKey).(*database.DB)
}

func AuthToken(ctx context.Context) string {
	token := ctx.Value(authTokenCtxKey)
	if token == nil {
		return ""
	}
	return token.(string)
}

func WithAuthToken(ctx context.Context, authToken string) context.Context {
	return context.WithValue(ctx, authTokenCtxKey, authToken)
}
