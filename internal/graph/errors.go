package graph

import "net/http"

type HTTPError struct {
	Code    int
	Message string
}

func NewHTTPError(code int) *HTTPError {
	return &HTTPError{Code: code, Message: http.StatusText(code)}
}

func (e *HTTPError) Error() string {
	return e.Message
}

var (
	Forbidden           = NewHTTPError(http.StatusForbidden)
	InternalServerError = NewHTTPError(http.StatusInternalServerError)
)
