package graph_test

import (
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	"myfresh/internal/database"
	"myfresh/internal/graph/model"
	"time"
)

var _ = Describe("SignInByCode", func() {
	expectedPhone := "9031234567"
	expectedCode := "1234"

	AfterEach(func() {
		truncateTable("signins")
		truncateTable("sessions, users")
	})

	Context("there is no sign in requests in database", func() {
		It("should reject authorization", func() {
			answer, sessions, users := signIn(expectedPhone, expectedCode)
			Expect(answer.Errors).NotTo(BeNil())
			Expect(sessions).To(HaveLen(0))
			Expect(users).To(HaveLen(0))
		})
	})

	Context("there was sign in request", func() {
		BeforeEach(func() {
			insert(&database.SignIn{
				Expiration: time.Now().Add(time.Hour),
				Phone:      expectedPhone,
				Code:       expectedCode,
			})
		})

		Context("sign in with wrong credentials", func() {
			unexpectedCode := "4321"
			unexpectedPhone := "9267654321"

			table.DescribeTable("should reject authorization",
				func(phone string, code string) {
					answer, sessions, users := signIn(phone, code)
					Expect(answer.Errors).NotTo(BeNil())
					Expect(sessions).To(HaveLen(0))
					Expect(users).To(HaveLen(0))
				},
				table.Entry("wrong credentials", unexpectedPhone, unexpectedCode),
				table.Entry("wrong phone", unexpectedPhone, expectedCode),
				table.Entry("wrong code", expectedPhone, unexpectedCode),
			)
		})

		Context("request is from new user", func() {
			It("should register new user", func() {
				answer, sessions, users := signIn(expectedPhone, expectedCode)
				Expect(answer.Errors).To(BeNil())
				Expect(users).To(HaveLen(1))
				Expect(sessions).To(HaveLen(1))

				user := users[0]
				session := sessions[0]

				Expect(user.Phone).To(Equal(expectedPhone))
				Expect(session.UserId).To(Equal(user.ID))
			})
		})

		Context("request is from registered user", func() {
			expectedUserId := 1

			BeforeEach(func() {
				insert(&database.User{
					ID:    expectedUserId,
					Phone: expectedPhone,
				})
			})

			It("should authorize user", func() {
				answer, sessions, users := signIn(expectedPhone, expectedCode)
				Expect(answer.Errors).To(BeNil())
				Expect(users).To(HaveLen(1))
				Expect(sessions).To(HaveLen(1))

				session := sessions[0]

				Expect(session.UserId).To(Equal(expectedUserId))
			})
		})
	})
})

type signInAnswer struct {
	Data   model.SignInPayload
	Errors interface{}
}

func signIn(phone string, code string) (*signInAnswer, []database.Session, []database.User) {
	var answer signInAnswer

	gql(`
      mutation {
        signInByCode(input: { phone: "`+phone+`", code: "`+code+`" }) {
          ... on SignInPayload {
            token
            viewer {
              user {
                phone
              }
            }
          }
          ... on ErrorPayload {
            message
          }
        }
      }
`, &answer)

	var sessions []database.Session
	err := db.Model(&sessions).Select()
	Expect(err).NotTo(HaveOccurred())

	var users []database.User
	err = db.Model(&users).Select()
	Expect(err).NotTo(HaveOccurred())

	return &answer, sessions, users
}
