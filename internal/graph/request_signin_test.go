package graph_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"myfresh/internal/database"
	"time"
)

var _ = Describe("RequestSignInCode", func() {
	AfterEach(func() { truncateTable("signins") })

	Context("when there is no signin requests in database", func() {
		firstPhone := "799999999"
		secondPhone := "799999998"

		It("should remember new request", func() {
			By("single attempt")

			signIns := requestSignIn(firstPhone)
			Expect(signIns).To(HaveLen(1))
			Expect(signIns[0].Phone).To(Equal(firstPhone))
			firstCode := signIns[0].Code

			By("double attempt")

			signIns = requestSignIn(firstPhone)
			Expect(signIns).To(HaveLen(1))
			Expect(signIns[0].Phone).To(Equal(firstPhone))
			Expect(signIns[0].Code).To(Equal(firstCode))

			By("another attempt")

			signIns = requestSignIn(secondPhone)
			Expect(signIns).To(HaveLen(2))
			Expect(signIns[0].Phone).To(Equal(firstPhone))
			Expect(signIns[1].Phone).To(Equal(secondPhone))
		})
	})

	Context("when there is expired signin request in database", func() {
		unexpectedCode := "0000"
		expectedPhone := "9991234567"

		BeforeEach(func() {
			insert(&[]database.SignIn{
				{Expiration: time.Now().Add(-time.Minute), Phone: expectedPhone, Code: unexpectedCode},
			})
		})

		It("should register new sign in", func() {
			signIns := requestSignIn(expectedPhone)
			Expect(signIns).To(HaveLen(1))
			Expect(signIns[0].Phone).To(Equal(expectedPhone))
			Expect(signIns[0].Code).NotTo(Equal(unexpectedCode))
		})
	})
})

type requestSigninAnswer struct {
	Data struct {
		RequestSignInCode interface{}
	}
	Errors interface{}
}

func requestSignIn(phone string) []database.SignIn {
	var answer requestSigninAnswer
	gql(`
              mutation {
                requestSignInCode(input: { phone: "`+phone+`" }) {
                  message
                }
              }
			`, &answer)
	Expect(answer.Errors).To(BeNil())

	var signIns []database.SignIn
	err := db.Model(&signIns).Select()
	Expect(err).NotTo(HaveOccurred())

	return signIns
}
