package graph_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	"myfresh/internal/database"
	"myfresh/internal/graph/model"
)

var _ = Describe("Products", func() {
	var answer productsAnswer

	BeforeEach(func() { answer = productsAnswer{} })
	AfterEach(func() { truncateTable("products") })

	Context("when there is no products in database", func() {
		It("should return empty list", func() {
			gql("query { products { id name } }", &answer)
			Expect(answer.Data.Products).To(HaveLen(0))
		})
	})

	Context("when there are products in database", func() {
		BeforeEach(func() {
			insert(&[]database.Product{
				{ID: 1, Name: "a"},
				{ID: 2, Name: "b"},
				{ID: 3, Name: "c"},
			})
		})

		DescribeTable("should return them all",
			func(query string, expected []*model.Product) {
				gql(query, &answer)
				Expect(answer.Data.Products).To(BeEquivalentTo(expected))
			},
			Entry("by all fields request",
				"query { products { id name } }",
				[]*model.Product{
					{ID: 1, Name: "a"},
					{ID: 2, Name: "b"},
					{ID: 3, Name: "c"},
				}),
			Entry("by partial fields request",
				"query { products { id } }",
				[]*model.Product{
					{ID: 1, Name: ""},
					{ID: 2, Name: ""},
					{ID: 3, Name: ""},
				}),
		)
	})
})

type productsAnswer struct {
	Data struct {
		Products []*model.Product
	}
}
