package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
	"github.com/go-pg/pg/v10"
	"math/rand"
	"myfresh/internal/database"
	"myfresh/internal/graph/generated"
	"myfresh/internal/graph/model"
	"myfresh/internal/reqctx"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

const SignInTTL = 10 * time.Minute
const SessionTTL = 30 * time.Minute

func (r *mutationResolver) RequestSignInCode(ctx context.Context, input model.RequestSignInCodeInput) (*model.ErrorPayload, error) {
	db := reqctx.DB(ctx)
	signIn, err := func() (*database.SignIn, error) {
		if err := r.deleteExpiredSignIns(ctx, db, input.Phone); err != nil {
			return nil, err
		}
		return r.rememberSignIn(ctx, db, input.Phone, SignInTTL)
	}()

	if err != nil {
		return &model.ErrorPayload{Message: err.Error()}, err
	}

	r.smsUser(ctx, signIn.Phone, signIn.Code)
	return nil, nil
}

func (r *mutationResolver) SignInByCode(ctx context.Context, input model.SignInByCodeInput) (model.SignInOrErrorPayload, error) {
	var token string
	var viewer model.Viewer

	db := reqctx.DB(ctx)
	err := func() error {
		if err := r.deleteExpiredSignIns(ctx, db, input.Phone); err != nil {
			return err
		}
		signIn, err := r.selectSignIn(ctx, db, input.Phone, input.Code)
		if err != nil {
			return err
		}
		if signIn == nil {
			return fmt.Errorf("authorization failed")
		}

		return reqctx.DB(ctx).RunInTransaction(ctx, func(tx *pg.Tx) error {
			if err := r.deleteSignIn(tx, input.Phone, input.Code); err != nil {
				return err
			}

			token = newToken(64)

			user, err := r.selectUser(tx, input.Phone)
			if err != nil {
				return err
			}
			if user == nil {
				user = &database.User{Phone: input.Phone}
				if _, err := tx.Model(user).Insert(); err != nil {
					return err
				}
			}

			if err := r.createSession(tx, user.ID, token, SessionTTL); err != nil {
				return err
			}

			viewer.User = &model.User{
				ID:    user.ID,
				Phone: user.Phone,
			}

			return nil
		})
	}()

	if err != nil {
		return &model.ErrorPayload{Message: err.Error()}, err
	}

	return model.SignInPayload{
		Token:  token,
		Viewer: &viewer,
	}, nil
}

func (r *mutationResolver) rememberSignIn(ctx context.Context, db *database.DB, phone string, ttl time.Duration) (*database.SignIn, error) {
	signIn := database.SignIn{
		Phone:      phone,
		Expiration: time.Now().Add(ttl),
		Code:       fmt.Sprintf("%04d", rand.Intn(9999)+1),
	}
	res, err := db.WithContext(ctx).Model(&signIn).OnConflict("DO NOTHING").Insert()
	if err != nil {
		return nil, err
	}
	n := res.RowsAffected()
	if err != nil {
		return nil, err
	}
	if n == 0 {
		if err := db.Model(&signIn).Where("phone = ?", phone).Limit(1).Select(); err != nil {
			return nil, err
		}
	}
	return &signIn, nil
}

func (r *mutationResolver) smsUser(_ context.Context, _ string, code string) {
	fmt.Printf("Code: %s\n", code)
}

func (r *mutationResolver) deleteExpiredSignIns(ctx context.Context, db *database.DB, phone string) error {
	_, err := db.WithContext(ctx).Model((*database.SignIn)(nil)).Where("phone = ? AND expiration <= ?", phone, time.Now()).Delete()
	return err
}

func (r *mutationResolver) selectSignIn(ctx context.Context, db *database.DB, phone string, code string) (*database.SignIn, error) {
	var signIns []database.SignIn
	if err := db.WithContext(ctx).Model(&signIns).Where("phone = ? AND code = ?", phone, code).Limit(1).Select(); err != nil {
		return nil, err
	}
	if len(signIns) == 0 {
		return nil, nil
	}
	return &signIns[0], nil
}

func (r *mutationResolver) deleteSignIn(tx *pg.Tx, phone string, code string) error {
	_, err := tx.Model((*database.SignIn)(nil)).Where("phone = ? AND code = ?", phone, code).Delete()
	return err
}

func (r *mutationResolver) selectUser(tx *pg.Tx, phone string) (*database.User, error) {
	var users []database.User
	if err := tx.Model(&users).Where("phone = ?", phone).Limit(1).Select(); err != nil {
		return nil, err
	}
	if len(users) == 0 {
		return nil, nil
	}

	return &users[0], nil
}

func (r *mutationResolver) createSession(tx *pg.Tx, userId int, token string, ttl time.Duration) error {
	session := database.Session{
		UserId:     userId,
		Expiration: time.Now().Add(ttl),
		Token:      token,
	}
	if _, err := tx.Model(&session).Insert(); err != nil {
		return err
	}
	return nil
}

func (r *queryResolver) Products(ctx context.Context) ([]*model.Product, error) {
	fields := graphql.CollectFieldsCtx(ctx, nil)
	var dbProducts []database.Product
	stmt := reqctx.DB(ctx).Model(&dbProducts)
	for _, f := range fields {
		stmt = stmt.Column(f.Name)
	}
	err := stmt.Select()
	if err != nil {
		return nil, err
	}

	products := make([]*model.Product, len(dbProducts))
	for i, p := range dbProducts {
		products[i] = &model.Product{
			ID:   p.ID,
			Name: p.Name,
		}
	}
	return products, nil
}

func (r *queryResolver) Viewer(ctx context.Context) (*model.Viewer, error) {
	db := reqctx.DB(ctx)
	token := reqctx.AuthToken(ctx)
	session, err := r.Session(ctx, db, token)
	if err != nil {
		return nil, InternalServerError
	}
	if session == nil {
		return nil, Forbidden
	}

	user, err := r.SelectViewer(ctx, db, session.UserId)
	if err != nil {
		return nil, InternalServerError
	}

	return &model.Viewer{
		User: &model.User{
			ID:    user.ID,
			Phone: user.Phone,
		},
	}, nil
}

func (r *queryResolver) Session(ctx context.Context, db *database.DB, token string) (*database.Session, error) {
	var sessions []database.Session
	if err := db.WithContext(ctx).Model(&sessions).Where("token = ?", token).Select(); err != nil {
		return nil, err
	}
	if len(sessions) == 0 {
		return nil, nil
	}
	return &sessions[0], nil
}

func (r *queryResolver) SelectViewer(ctx context.Context, db *database.DB, id int) (*database.User, error) {
	var user database.User
	if err := db.WithContext(ctx).Model(&user).Where("id = ?", id).Limit(1).Select(); err != nil {
		return nil, err
	}
	return &user, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }

func newToken(n int) string {
	token := make([]byte, n)
	for i := 0; i < n; i++ {
		token[i] = randChar()
	}
	return string(token)
}

func randChar() byte {
	n := byte(rand.Intn(62))
	if n < 10 {
		return n + '0'
	}
	n -= 10
	if n < 26 {
		return n + 'A'
	}
	return n - 26 + 'a'
}
