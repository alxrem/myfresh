package graph_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"myfresh/internal/database"
	"myfresh/internal/graph/model"
	"time"
)

var _ = Describe("Viewer", func() {
	expectedToken := "0123456789012345678901234567890123456789012345678901234567890123"
	expectedPhone := "9031234567"

	unexpectedToken := "0123456789012345678901234567890123456789012345678901234567893210"

	BeforeEach(func() {
		insert(&database.User{ID: 1, Phone: expectedPhone})
		insert(&database.Session{ID: 1, UserId: 1, Expiration: time.Now().Add(time.Hour), Token: expectedToken})
	})

	AfterEach(func() {
		truncateTable("sessions, users")
	})

	It("should forbid request", func() {
		By("without token")
		answer := viewer()
		Expect(answer.Errors).NotTo(BeNil())

		By("invalid token")
		answer = viewer(unexpectedToken)
		Expect(answer.Errors).NotTo(BeNil())
	})

	It("should receive viewer with valid token", func() {
		answer := viewer(expectedToken)
		Expect(answer.Errors).To(BeNil())
		Expect(answer.Data.Viewer.User.Phone).To(Equal(expectedPhone))
	})
})

type viewerAnswer struct {
	Data struct {
		Viewer model.Viewer
	}
	Errors interface{}
}

func viewer(token ...string) *viewerAnswer {
	var answer viewerAnswer

	gql(`
      query {
        viewer {
          user {
            phone
          }
        }
      }
`, &answer, token...)

	return &answer
}
