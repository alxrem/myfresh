package graph_test

import (
	"bytes"
	"encoding/json"
	"github.com/go-pg/pg/extra/pgdebug/v10"
	"myfresh/internal/database"
	"myfresh/internal/options"
	"myfresh/internal/server"
	"net/http"
	"net/http/httptest"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestGraph(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Graph Suite")
}

var db *database.DB
var srv *server.Server

var _ = BeforeSuite(func() {
	opts, err := options.Parse()
	Expect(err).NotTo(HaveOccurred())

	db, err = database.NewDB(&opts.DB)
	Expect(err).NotTo(HaveOccurred())

	db.AddQueryHook(pgdebug.NewDebugHook())
	Expect(err).NotTo(HaveOccurred())

	srv = server.NewServer(&opts.HTTP, db)

	truncateTable("products")
	truncateTable("signins")
	truncateTable("users, sessions")
})

func truncateTable(table string) {
	_, err := db.Exec("TRUNCATE " + table)
	Expect(err).NotTo(HaveOccurred())
}

func insert(model interface{}) {
	_, err := db.Model(model).Insert()
	Expect(err).NotTo(HaveOccurred())
}

func gql(q string, answer interface{}, token ...string) {
	body, err := json.Marshal(&struct{ Query string }{q})
	Expect(err).NotTo(HaveOccurred())
	req, err := http.NewRequest("POST", "http://localhost:8080/query", bytes.NewReader(body))
	Expect(err).NotTo(HaveOccurred())
	req.Header.Set("Content-Type", "application/json")
	if len(token) > 0 {
		req.Header.Set("Authorization", "bearer "+token[0])
	}

	w := httptest.NewRecorder()
	srv.ServeHTTP(w, req)
	Expect(w.Result().StatusCode).To(Equal(http.StatusOK))

	err = json.Unmarshal(w.Body.Bytes(), answer)
	Expect(err).NotTo(HaveOccurred())
}
