module myfresh

go 1.16

require (
	github.com/99designs/gqlgen v0.14.0
	github.com/caarlos0/env/v6 v6.8.0
	github.com/go-pg/pg/extra/pgdebug/v10 v10.10.6
	github.com/go-pg/pg/v10 v10.10.6
	github.com/onsi/ginkgo v1.16.5
	github.com/onsi/gomega v1.10.3
	github.com/sirupsen/logrus v1.8.1
	github.com/vektah/gqlparser/v2 v2.2.0
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871 // indirect
	golang.org/x/sys v0.0.0-20211124211545-fe61309f8881 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
