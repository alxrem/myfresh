-- migrate:up
CREATE TABLE IF NOT EXISTS products
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS users
(
    id    SERIAL PRIMARY KEY,
    phone VARCHAR(255) NOT NULL,

    UNIQUE (phone)
);

CREATE TABLE IF NOT EXISTS signins
(
    id         SERIAL PRIMARY KEY,
    expiration TIMESTAMP NOT NULL,
    phone      VARCHAR(255) NOT NULL,
    code       VARCHAR(255) NOT NULL,

    UNIQUE (phone)
);

CREATE TABLE IF NOT EXISTS sessions
(
    id         SERIAL PRIMARY KEY,
    user_id    INTEGER NOT NULL REFERENCES users (id),
    expiration TIMESTAMP NOT NULL,
    token      VARCHAR(255) NOT NULL
);

-- migrate:down
DROP TABLE IF EXISTS sessions;
DROP TABLE IF EXISTS signins;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS products;
