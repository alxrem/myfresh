-- migrate:up

INSERT INTO products (id, name) VALUES (1, 'Apple'), (2, 'Milk'), (3, 'Meat');
INSERT INTO users (id, phone) VALUES (1, '79031234567'), (2, '79262345678');

-- migrate:down

TRUNCATE products, users;
