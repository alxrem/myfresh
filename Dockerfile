FROM alpine AS external
RUN wget https://github.com/amacneil/dbmate/releases/download/v1.12.1/dbmate-linux-amd64 -qO dbmate && chmod +x dbmate

FROM golang:1.16-alpine AS builder

ENV CGO_ENABLED=0

WORKDIR /src
COPY go.mod go.sum ./
RUN go mod download
COPY *.go ./
COPY internal internal/
RUN go build

FROM alpine
COPY --from=external /dbmate /usr/local/bin/
COPY --from=builder /src/myfresh /usr/local/bin/
COPY migrations /db/migrations/
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["dbmate --wait --wait-timeout=10s up && exec myfresh"]